using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToTopLeft : MonoBehaviour
{
    void Start()
    {

    }

    void Update() {
        int w = Camera.main.pixelWidth;
        int h = Camera.main.pixelHeight;
        Vector3 scale = transform.parent.transform.parent.localScale;
        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2((-w/2)/scale.x,(h/2)/scale.y);
    }
}
