using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Dictionaries {
    public static Dictionary<int, string> cardNames = new Dictionary<int, string>() {
        {1, "A"}, {2, "2"}, {3,"3"}, {4, "4"}, {5, "5"}, {6, "6"}, {7, "7"}, {8, "8"}, {9, "9"}, {10, "10"}, {11, "J"}, {12, "Q"}, {13, "K"} 
    };
    public static Dictionary<int, string> states = new Dictionary<int, string>() {
        {0, ""}, {1, "Win"}, {-1, "Lose"}, {-2, "Bust"}
    };
    public static Dictionary<int, char> colours = new Dictionary<int, char>() {
        {0, 'H'}, {1, 'D'}, {2, 'C'}, {3, 'S'}
    };
}

public class BlackJack : MonoBehaviour
{
    public Texture statsMenuTexture;


    List<Card>[] hands = new List<Card>[0];
    List<Card> dealerHand = new List<Card>();
    List<Card> deck = new List<Card>();
    List<Card> discardPile = new List<Card>();
    
    int viewWidth;
    int viewHeight;
    int currentPlayer;
    int[] playerStates;
    int playerCount = 1;
    int deckCount = 1;
    int totalCards;
    int value;
    char colour;
    float sw = 0;
    float sh = 0;
    bool gameOver = false;
    bool isActive = false;
    Vector2 lastMousePos;
    DragMenu statsMenu = new DragMenu(0,0,0,0,0,0);
    bool dragMenu = false;
    GUIStyle styleStatsCentered = new GUIStyle();
    GUIStyle styleStats = new GUIStyle();
    GUIStyle styleBig = new GUIStyle();
    GUIStyle styleSmall = new GUIStyle();

    GameObject cardsGameObject;


    // Start is called before the first frame update
    public void Start()
    {
        viewWidth = GetComponent<Camera>().pixelWidth;
        viewHeight = GetComponent<Camera>().pixelHeight;
        sw = viewWidth/100;
        sh = viewHeight/100;
        statsMenu = new DragMenu(viewWidth-viewHeight*0.144f,0,viewHeight*2,viewHeight, viewWidth, viewHeight);
        
        styleStatsCentered.fontSize = (int)(3*sw);
        styleStatsCentered.normal.textColor = Color.white;
        styleStatsCentered.alignment = TextAnchor.UpperCenter;
        styleStats.fontSize = (int)(3*sw);
        styleStats.normal.textColor = Color.white;
        styleBig.fontSize = (int)(2*sw);
        styleBig.normal.textColor = Color.white;
        styleBig.alignment = TextAnchor.MiddleCenter;
        styleSmall.fontSize = (int)(2*sw);
        styleSmall.normal.textColor = Color.white;

        cardsGameObject = GameObject.Find("Cards");

        Reset(playerCount);
    }

    // Update is called once per frame
    void Update()
    {
        statsMenu.Update();
        lastMousePos = Input.mousePosition;
    }
    
    void OnGUI()
    {
        if(isActive) {
            GUI.DrawTexture(statsMenu.rect, statsMenuTexture, ScaleMode.StretchToFill);
            
            DrawButtons();
            
            DrawStats();
        }
    }

    public void SetActive(bool activate) {
        isActive = activate;
    }
    void DrawButtons() {
        if(GUI.Button(new Rect(2*sh, viewHeight - 42*sh, 40*sh, 12*sh), "")) {
            addCard();
        }
        if (GUI.Button(new Rect(2*sh, viewHeight - 42*sh, 40*sh, 12*sh), "Add Card", styleBig)) {}
        if(GUI.Button(new Rect(2*sh, viewHeight - 28*sh, 40*sh, 12*sh), "")) {
            NextRound();
        }
        if (GUI.Button(new Rect(2*sh, viewHeight - 28*sh, 40*sh, 12*sh), "NEXT ROUND", styleBig)) {}
        if(GUI.Button(new Rect(2*sh, viewHeight - 14*sh, 40*sh, 12*sh), "")) {
                currentPlayer++;
        }
        if (GUI.Button(new Rect(2*sh, viewHeight - 14*sh, 40*sh, 12*sh), "HOLD", styleBig)) {}       
    }

    void DrawStats() {
        GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f, 10*sh, 43.5f*sw, 3*sw), String.Format("Discard pile:    {0}", discardPile.Count), styleStats);
        GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f, 18*sh, 43.5f*sw, 3*sw), String.Format("Draw pile:    {0}", deck.Count), styleStats);
        int[] cards = new int[13];
        foreach (Card c in deck) {
            cards[c.value-1]++;
        }
        for (int i = 1; i<=13; i++) {
            /*if((i-1)%2==0) fill(0);
            else fill(255,0,0);*/
            GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f+(i-1)*3*sw, 29f*sh, 3f*sw, 3*sw), String.Format("{0}", Dictionaries.cardNames[i]), styleStatsCentered);
            //fill(255);
            GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f+(i-1)*3*sw, 37f*sh, 3f*sw, 3*sw), String.Format("{0}", cards[i-1]), styleStatsCentered);
        }
        
        // probability face cards/tens
        double permille = deck.Count>0 ? Math.Round(((double)(cards[9] + cards[10] + cards[11] + cards[12])/deck.Count*1000)) : -1;
        GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f, 48*sh, 45*sw, 3*sw), String.Format("Value 10 Cards:  {0}%  (average=30.8%)", (permille/10)), styleSmall);
        
        // probability to get busted
        if(currentPlayer<hands.Length) {
            int space = 21 - GetMinScore(hands[currentPlayer]);
            int bustCards = 0;
            for(int i = 0; i<deck.Count; i++) {
                if(Mathf.Clamp(deck[i].value, 1, 10) > space) {
                    bustCards++;
                }
            }
            permille = deck.Count>0 ? Math.Round(((double)bustCards/deck.Count*1000)) : -1;
            GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f, 54f*sh, 45*sw, 3*sw), String.Format("Probability to bust on hit:  {0}%", (permille/10)), styleSmall);
        }
        else {
            GUI.Label(new Rect(statsMenu.rect.x + viewHeight*0.2f, 54f*sh, 45*sw, 3*sw), String.Format("Probability to bust on hit:  -"), styleSmall);
        }

        // hand cards
        DrawDealerStats(dealerHand, statsMenu.rect.x + viewHeight*0.2f, 66f*sh);
        DrawAllPlyStats(hands, statsMenu.rect.x + viewHeight*0.2f, 78f*sh);
    }

    void Hit() {
        if (!gameOver) {
            ShuffleIfNecessary();
            deck.Shuffle();
            Card nextCard = deck[deck.Count-1];
            Hit(nextCard.value, nextCard.colour);
        }
    }

    public void Hit(String card) {
        String[] valueColour = card.Split('-');
        value = Int32.Parse(valueColour[0]);
        colour = Char.ToUpper(valueColour[1][0]);
    }

    public void addCard() {
        Hit(value, colour);
    }

    public void Hit(int value, char colour) {
        if (!deck.Contains(new Card(value, colour)))
            return;

        if (!gameOver) {
            if (currentPlayer<playerCount) {
                hands[currentPlayer].Add(new Card(value, colour));
                deck.Remove(new Card(value, colour));
                if (GetScore(hands[currentPlayer])>21 && hands[currentPlayer].Count!=2) {
                    playerStates[currentPlayer] = -2;
                    currentPlayer++;
                }
            } else {
                DealerHit(value, colour);
                if (GetScore(dealerHand)>=17) {
                    GameOver();
                }
            }
        }
    }

    void GameOver() {
        gameOver = true;
        int dealerScore = GetScore(dealerHand);
        for (int i = 0; i<hands.Length; i++) {
            int score = GetScore(hands[i]);
            if (score>21 && score!=100) {
            playerStates[i] = -2;
            } else if (score>dealerScore) {
            playerStates[i] = 1;
            } else if (score==dealerScore) {
            playerStates[i] = 0;
            } else if (score<dealerScore) {
            if (dealerScore>21 && dealerScore!=100) {
                playerStates[i] = 1;
            } else {
                playerStates[i] = -1;
            }
            }
        }
    }

    void DealerHit() {
        ShuffleIfNecessary();
        Card nextCard = deck[deck.Count-1];
        DealerHit(nextCard.value, nextCard.colour);
    }

    void DealerHit(int value, char colour) {
        if(dealerHand.Count==0) {
            currentPlayer = 0;
        }
        dealerHand.Add(new Card(value, colour));
        deck.Remove(new Card(value, colour));
    }

    void ShuffleIfNecessary() {
        if (deck.Count<=52) {
            ShuffleDeck();
        }
    }

    void Hold() {
    }

    public void Reset(int players) {
        playerCount = players;
        InitializeDeck();
        ClearTable();
        discardPile.Clear();
        ShuffleDeck();
        NextRound();
    }

    void NextRound() {
        gameOver = false;
        ClearTable();
        currentPlayer = playerCount;
        // initialize player states
        playerStates = new int[playerCount];
    }

    void ClearTable() {
        discardPile.AddRange(dealerHand);
        dealerHand.Clear();
        foreach (List<Card> hand in hands) {
            discardPile.AddRange(hand);
            hand.Clear();
        }
    }

    void InitializeDeck() {
        deck.Clear();
        // initialize player hands
        hands = new List<Card>[playerCount];
        for (int i = 0; i<hands.Length; i++) {
            hands[i] = new List<Card>();
        }
        for (int numOfDecks = 0; numOfDecks<deckCount; numOfDecks++) {
            for (int i = 1; i<=13; i++) {
                for (int j = 0; j<4; j++) {
                    deck.Add(new Card(i, Dictionaries.colours[j]));
                }
            }
        }
        totalCards = deck.Count;
    }

    void ShuffleDeck() {
        deck.AddRange(discardPile);
        discardPile.Clear();
        deck.Shuffle();
    }

    int GetScore(List<Card> hand) {
        int score = 0;
        int aces = 0;
        foreach (Card c in hand) {
            if (c.value == 1) {
                aces++;
                score += 11;
            } else {
                score += c.value.Clamp(2, 10);
            }
        }
        for (int i = 0; i<aces; i++) {
            if (score>21) {
                score -= 10;
            }
        }
        if(score==21 && hand.Count==2) {
            score = 100;
        }
        return score;
    }

    int GetMinScore(List<Card> hand) {
        int score = 0;
        foreach (Card c in hand) {
            score += c.value.Clamp(1, 10);
        }
        return score;
    }

    void DrawDealerStats(List<Card> hand, float x, float y) {
        int score = GetScore(hand);
        GUI.Label(new Rect(x, y-2.2f*sw, 30*sw, 3*sw), score==100 ? "Natural 21" : String.Format("{0}", score), styleSmall);
        GUI.Label(new Rect(x + 12*sw, y-2.2f*sw, 30*sw, 3*sw), String.Format("{0} (Dealer)", GetScore(hand)>21 ? Dictionaries.states[-2] : Dictionaries.states[0]), styleSmall);
        for(int i = 0; i<hand.Count; i++) {
            GUI.Label(new Rect(x + i*3*sw, y, 30*sw, 3*sw), String.Format("{0}", Dictionaries.cardNames[hand[i].value]), styleSmall);
        }
    }

    void DrawPlyStats(List<Card> hand, float x, float y, int player) {
        int score = GetScore(hand);
        GUI.Label(new Rect(x, y-2.2f*sw, 30*sw, 3*sw), score==100 ? "Natural 21" : String.Format("{0}", score), styleSmall);
        string state = Dictionaries.states[playerStates[player]];
        if(playerStates[player]==0 && gameOver) {
            state = "Push";
        }
        GUI.Label(new Rect(x + 12*sw, y-2.2f*sw, 30*sw, 3*sw), String.Format("{0} (Player {1})", state, player+1), styleSmall);
        for(int i = 0; i<hand.Count; i++) {
            GUI.Label(new Rect(x +i*3*sw, y, 30*sw, 3*sw), String.Format("{0}", Dictionaries.cardNames[hand[i].value]), styleSmall);
        }
    }

    void DrawAllPlyStats(List<Card>[] hands, float x, float y) {
        for(int i = 0; i< hands.Length; i++) {
            DrawPlyStats(hands[i], x, y+i*6*sw, i);
        }
    }

    bool RemoveCard(int value) {
        for(int i = 0; i<deck.Count; i++) {
            if(deck[i].value==value) {
                deck.RemoveAt(i);
                return true;
            }
        }
        // no card with given value
        return false;
    }

    public void ChangePlayerCount(int change)
    {
        if (playerCount == 1 && change == -1)
            return;

        playerCount += change;
        GameObject.Find("PlayerCount").GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Players: " + playerCount.ToString();
    }

    public void ChangeDeckCount(int change)
    {
        if (deckCount == 1 && change == -1)
            return;

        deckCount += change;
        GameObject.Find("DeckCount").GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Decks: " + deckCount.ToString();
    }
}

class Card : IComparable {
    public int value;
    public char colour;
    public Card(int value, char colour) {
        this.value = value;
        this.colour = colour;
    }
    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        
        if(this.value==((Card)obj).value && this.colour==((Card)obj).colour) {
            return true;
        }
        else {
            return false;
        } 
    }
    public override int GetHashCode()
    {
        return this.value<<16 + this.colour;
    }
    public override string ToString()
    {
        return String.Format("Card({0},{1})", Dictionaries.cardNames[this.value]);
    }
    public int CompareTo(object o) {
        return value - ((Card)o).value;
    }
}

class DragMenu {
    public Rect rect = new Rect();
    float xSpeed = 0;
    bool dragMenu = false;
    int viewWidth;
    int viewHeight;
    float anchorX;
    float[] stopX;
    public DragMenu(float x, float y, float width, float height, int viewWidth, int viewHeight) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        rect = new Rect(x,y,width,height);
        stopX = new float[] {viewWidth/2, x};
    }
    public void Update() {
        if(Input.GetMouseButtonDown(0)) {
            if(rect.Contains(Input.mousePosition)){
                dragMenu = true;
                anchorX = Input.mousePosition.x - rect.x;
            }
        }
        if(Input.GetMouseButtonUp(0)) {
            dragMenu = false;
        }
        if(dragMenu) {
            xSpeed = (Input.mousePosition.x - (anchorX+rect.x)) * 0.4f;
        }
        else {
            int closest = 0;
            for(int i = 1; i<stopX.Length; i++) {
                float val = Math.Abs((rect.x + xSpeed*10) - stopX[i]);
                if (val < Math.Abs((rect.x + xSpeed*10) - stopX[closest])) {
                    closest = i;
                }
            }
            xSpeed = xSpeed + (stopX[closest] - (rect.x + xSpeed*3)) * 0.2f;
        }
        rect.x = Mathf.Clamp(rect.x + xSpeed, viewWidth/2, viewWidth-viewHeight*0.144f);
    }
}
